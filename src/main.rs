extern crate args;
extern crate getopts;
extern crate serde_json;

use std::collections::HashMap;
use std::env;
use std::io;
use std::io::prelude::*;
use std::process::Command;
use std::process::exit;
use std::str;
use std::str::FromStr;

use serde::{Serialize,Deserialize};

use getopts::Occur;

use args::{Args,ArgsError};

const PROGRAM_DESC: &'static str = "Check for known security issues in Arch Linux package lists.\n\nExample:\n    pacman -Q | secman";
const PROGRAM_NAME: &'static str = "secman";

enum Output {
    Json,
    Name,
    Url,
}

impl FromStr for Output {

    type Err = ArgsError;

    fn from_str(input: &str) -> Result<Output, Self::Err> {
        match input {
            "name"  => Ok(Output::Name),
            "json"  => Ok(Output::Json),
            "url"  => Ok(Output::Url),
            _      => Err(ArgsError::new("", "invalid value for output")),
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
struct Avg {
    name: String,
    packages: Vec<String>,
    status: String,
    severity: String,
    #[serde(rename = "type")]
    vulntype: String,
    affected: String,
    fixed: Option<String>,
    ticket: Option<String>,
    issues: Vec<String>,
    advisories: Vec<String>,
}

// < 0 : if ver1 < ver2
// = 0 : if ver1 == ver2
// > 0 : if ver1 > ver2
fn vercmp(ver1: &str, ver2: &str) -> i32 {
    let output = Command::new("vercmp")
            .arg(ver1)
            .arg(ver2)
            .output()
            .expect("failed to execute vercmp");
    let res = str::from_utf8(&output.stdout).expect("Failed to read vercmp output");
    let val: i32 = res.trim().parse().expect("Failed to parse vercmp output");
    val
}

fn is_affected(package: &String, affected_version: &String, installed_pkgs: &HashMap<String, String>) -> bool {
    match installed_pkgs.get(package) {
        Some(installed_version) => vercmp(installed_version, affected_version) <= 0,
        None => false,
    }
}

fn is_relevant<'a>(avg: &'a Avg, installed_pkgs: &HashMap<String, String>) -> Option<&'a Avg> {
    if avg.packages.iter().any(|pkg| is_affected(pkg, &avg.affected, installed_pkgs)) {
        Some(avg)
    } else {
        None
    }
}

fn parse_args(input: &Vec<String>) -> Output {
    let mut args = Args::new(PROGRAM_NAME, PROGRAM_DESC);
    args.flag("h", "help", "Print the usage menu");
    args.option("o",
        "output",
        "The output format, one of 'name', 'json', 'url' (default: 'url')",
        "OUTPUT",
        Occur::Optional,
        Some(String::from("url")));

    match args.parse(input) {
        Err(e) => {
            eprintln!("{}", e);
            eprintln!("");
            eprintln!("{}", args.full_usage());
            exit(1); 
        },
        _ => (),
    };

    let help = args.value_of("help");
    match help {
        Ok(true) => { 
            println!("{}", args.full_usage()); 
            exit(0); 
        },
        _ => (),
    };

    let output: Result<Output, ArgsError> = args.value_of("output");
    match output {
        Err(e) => {
            eprintln!("{}", e);
            eprintln!("");
            eprintln!("{}", args.full_usage());
            exit(1);
        },
        Ok(o) => return o,
    }
}

fn read_installed_pkgs() -> HashMap<String, String> {
    let mut packages = HashMap::new();

    let stdin = io::stdin();
    for line in stdin.lock().lines() {
        let val = line.unwrap();
        let mut iter = val.split_ascii_whitespace();
        let package = iter.next();
        let version = iter.next(); //.expect("malformed input");
        match (package, version) {
            (Some(p), Some(v)) => packages.insert(String::from(p), String::from(v)),
            _ => {
                eprintln!("Failed to parse package version: {}", val);
                exit(1);
            }
        };
    }
    packages
}

fn read_avgs() -> Vec<Avg> {
    let req = ureq::get("https://security.archlinux.org/all.json")
        .set("user-agent", "secman 0.1.0 https://anemos.io/projects/secman");

    let resp = match req.call() {
        Ok(r) => r,
        Err(e) => {
            eprintln!("Failed to retrieve AVGs: {}", e);
            exit(1);
        }
    };
    let all_avgs: Vec<Avg> = match resp.into_json() {
        Ok(avgs) => avgs,
        Err(e) => {
            eprintln!("Failed to parse AVG JSON: {}", e);
            exit(1);
        }
    };
    all_avgs
}

fn main() {
    let output_mode = parse_args(&env::args().collect());
    
    let installed_pkgs = read_installed_pkgs();
    let all_avgs = read_avgs();
    let relevant_avgs = all_avgs.iter().filter_map(|avg| is_relevant(avg, &installed_pkgs));
    
    if let Output::Json = output_mode {
        let result: Vec<&Avg> = relevant_avgs.collect();
        println!("{}", serde_json::to_string_pretty(&result).expect("JSON output error"));
        exit(0);
    }

    for avg in relevant_avgs {
        match output_mode {
            Output::Name => println!("{}", avg.name),
            Output::Url => println!("https://security.archlinux.org/{}", avg.name),
            Output::Json => (),
        };
    }
 }
