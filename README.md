# secman

Check for known security issues in Arch Linux package lists.

* [Man page](https://anemos.io/man/secman.1.html)
* [Project homepage](https://anemos.io/projects/secman)

## Building

* Executable: `cargo build --release --locked`
* Man page: `make -C doc secman.1` 
* Man page (html): `make -C doc secman.1.html` 
